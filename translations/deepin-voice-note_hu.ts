<?xml version="1.0" ?><!DOCTYPE TS><TS language="hu" version="2.1">
<context>
    <name>AppMain</name>
    <message>
        <location filename="../src/main.cpp" line="46"/>
        <location filename="../src/views/vnotemainwindow.cpp" line="654"/>
        <source>Voice Notes</source>
        <translation>Hangjegyzetek</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="47"/>
        <source>Voice Notes is a lightweight memo tool to make text notes and voice recordings.</source>
        <translation>A Hangjegyzetek egy könnyű jegyzeteszköz szöveges jegyzetek és hangfelvételek készítéséhez.</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="655"/>
        <source>Recordings not saved</source>
        <translation>Felvétel nincs mentve</translation>
    </message>
</context>
<context>
    <name>DefaultName</name>
    <message>
        <location filename="../src/db/vnotefolderoper.cpp" line="193"/>
        <source>Notebook</source>
        <translation>Jegyzettömb</translation>
    </message>
    <message>
        <location filename="../src/db/vnoteitemoper.cpp" line="194"/>
        <source>Text</source>
        <translation>Szöveg</translation>
    </message>
    <message>
        <location filename="../src/db/vnoteitemoper.cpp" line="205"/>
        <location filename="../src/importolddata/olddbvisistors.cpp" line="132"/>
        <source>Voice</source>
        <translation>Hang</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../src/views/homepage.cpp" line="41"/>
        <source>Create Notebook</source>
        <translation>Jegyzettömb létrehozása</translation>
    </message>
    <message>
        <location filename="../src/views/homepage.cpp" line="53"/>
        <source>Create a notebook to start recording voice and making notes</source>
        <translation>Hozzon létre egy jegyzetfüzetet a hangfelvétel és a jegyzetek készítéséhez</translation>
    </message>
</context>
<context>
    <name>MiddleView</name>
    <message>
        <location filename="../src/views/middleview.cpp" line="164"/>
        <location filename="../src/views/middleview.cpp" line="199"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../src/views/middleview.cpp" line="294"/>
        <source>No search results</source>
        <translation>Nincs keresési találat</translation>
    </message>
</context>
<context>
    <name>NoteDetailContextMenu</name>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="172"/>
        <source>Save as MP3</source>
        <translation>Mentés MP3 fájlként</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="173"/>
        <source>Voice to Text</source>
        <translation>Hangból szöveg</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="174"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="175"/>
        <source>Select all</source>
        <translation>Összes kiválasztása</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="176"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="177"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="178"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="179"/>
        <source>Text to Speech</source>
        <translation>Szövegből beszéd</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="180"/>
        <source>Stop reading</source>
        <translation>Olvasás megállítása</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="181"/>
        <source>Speech to Text</source>
        <translation>Beszédből szöveg</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="182"/>
        <source>Translate</source>
        <translation>Fordítás</translation>
    </message>
</context>
<context>
    <name>NotebookContextMenu</name>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="130"/>
        <source>Rename</source>
        <translation>Átnevezés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="131"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="132"/>
        <source>New note</source>
        <translation>Új feljegyzés</translation>
    </message>
</context>
<context>
    <name>NotesContextMenu</name>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="150"/>
        <source>Rename</source>
        <translation>Átnevezés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="151"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="152"/>
        <source>Save as TXT</source>
        <translation>Mentés TXT fájlként</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="153"/>
        <source>Save voice recording</source>
        <translation>Hangfelvétel mentése</translation>
    </message>
    <message>
        <location filename="../src/common/actionmanager.cpp" line="154"/>
        <source>New note</source>
        <translation>Új feljegyzés</translation>
    </message>
</context>
<context>
    <name>RightView</name>
    <message>
        <location filename="../src/views/rightview.cpp" line="78"/>
        <source>The voice note has been deleted</source>
        <translation>Hangjegyzet törölve</translation>
    </message>
    <message>
        <location filename="../src/views/rightview.cpp" line="86"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/views/rightview.cpp" line="1153"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
</context>
<context>
    <name>Setting</name>
    <message>
        <location filename="../src/common/setting.cpp" line="37"/>
        <source>Basic</source>
        <translation>Alap</translation>
    </message>
    <message>
        <location filename="../src/common/setting.cpp" line="38"/>
        <source>Audio Source</source>
        <translation>Hangforrás</translation>
    </message>
    <message>
        <location filename="../src/common/setting.cpp" line="39"/>
        <source>Internal</source>
        <translation>Belső</translation>
    </message>
    <message>
        <location filename="../src/common/setting.cpp" line="40"/>
        <source>Microphone</source>
        <translation>Mikrofon</translation>
    </message>
</context>
<context>
    <name>Shortcuts</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="947"/>
        <source>New notebook</source>
        <translation>Új jegyzetfüzet</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="948"/>
        <source>Rename notebook</source>
        <translation>Jegyzetfüzet átnevezése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="949"/>
        <source>Delete notebook</source>
        <translation>Jegyzetfüzet törlése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="971"/>
        <source>New note</source>
        <translation>Új feljegyzés</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="972"/>
        <source>Rename note</source>
        <translation>Feljegyzés átnevezése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="973"/>
        <source>Delete note</source>
        <translation>Feljegyzés törlése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="974"/>
        <source>Play/Pause</source>
        <translation>Lejátszás/Szünet</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="975"/>
        <source>Record voice</source>
        <translation>Hang rögzítése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="976"/>
        <source>Voice to Text</source>
        <translation>Hangból szöveg</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="977"/>
        <source>Save as MP3</source>
        <translation>Mentés MP3 fájlként</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="978"/>
        <source>Save as TXT</source>
        <translation>Mentés TXT fájlként</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="979"/>
        <source>Save recordings</source>
        <translation>felvételek mentése</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="999"/>
        <source>Select all</source>
        <translation>Összes kiválasztása</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1000"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1001"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1002"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1003"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1026"/>
        <source>Help</source>
        <translation>Segítség</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1027"/>
        <source>Display shortcuts</source>
        <translation>Parancsikonok megjelenítése</translation>
    </message>
</context>
<context>
    <name>ShortcutsGroups</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="953"/>
        <source>Notebooks</source>
        <translation>Jegyzetfüzetek</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="983"/>
        <source>Notes</source>
        <translation>Jegyzetek</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1007"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1031"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
</context>
<context>
    <name>SplashView</name>
    <message>
        <location filename="../src/views/splashview.cpp" line="42"/>
        <source>Loading...</source>
        <translation>Betöltés...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="449"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1762"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1764"/>
        <source>Privacy Policy</source>
        <translation>Adatvédelmi irányelvek</translation>
    </message>
</context>
<context>
    <name>UpgradeView</name>
    <message>
        <location filename="../src/importolddata/upgradeview.cpp" line="46"/>
        <source>Importing notes from the old version, please wait...</source>
        <translation>Jegyzetek importálása a régi verzióból, kérjük, várjon ...</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../src/common/utils.cpp" line="45"/>
        <source>1 min ago</source>
        <translation>1 perccel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/common/utils.cpp" line="47"/>
        <source>%1 mins ago</source>
        <translation>%1 perccel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/common/utils.cpp" line="53"/>
        <source>Yesterday</source>
        <translation>Tegnap</translation>
    </message>
</context>
<context>
    <name>VNoteErrorMessage</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="909"/>
        <source>The voice conversion failed due to the poor network connection. Do you want to try again?</source>
        <translation>A hangkonvertálás a gyenge hálózati kapcsolat miatt nem sikerült. Megpróbálja újra?</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="914"/>
        <source>The voice conversion failed. Do you want to try again?</source>
        <translation>A hangkonvertálás nem sikerült. Megpróbálja újra?</translation>
    </message>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1616"/>
        <source>Try Again</source>
        <translation>Próbálja újra</translation>
    </message>
</context>
<context>
    <name>VNoteMainWindow</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="528"/>
        <source>Create Notebook</source>
        <translation>Jegyzettömb létrehozása</translation>
    </message>
</context>
<context>
    <name>VNoteMessageDialog</name>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="44"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="116"/>
        <source>Cancel</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="117"/>
        <source>Confirm</source>
        <translation>Megerősít</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="120"/>
        <source>Are you sure you want to delete this notebook?
All notes in it will be deleted</source>
        <translation>Biztosan törli ezt a jegyzetfüzetet?
Az összes jegyzet törlődik</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="124"/>
        <source>Do you want to stop the current recording?</source>
        <translation>Le akarja állítani az aktuális felvételt?</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="128"/>
        <source>Are you sure you want to delete this note?</source>
        <translation>Biztosan törli ezt a jegyzetet?</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="133"/>
        <source>Cannot convert this voice note, as notes over 20 minutes are not supported at present.</source>
        <translation>Nem lehet konvertálni ezt a hangjegyzetet, mivel a 20 percnél hosszabb jegyzeteket jelenleg nem támogatjuk.</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="138"/>
        <source>Converting a voice note now. Do you want to stop it?</source>
        <translation>Hangjegyzet konvertálása folyamatban. Meg akarja állítani?</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="142"/>
        <source>The low input volume may result in bad recordings. Do you want to continue?</source>
        <translation>Az alacsony bemeneti hangerő rossz felvételeket eredményezhet. Akarja folytatni?</translation>
    </message>
    <message>
        <location filename="../src/dialog/vnotemessagedialog.cpp" line="146"/>
        <source>The clipped recordings and converted text will not be pasted. Do you want to continue?</source>
        <translation>A levágott felvételeket és az átalakított szöveget nem illesztjük be. Akarja folytatni?</translation>
    </message>
</context>
<context>
    <name>VNoteRecordBar</name>
    <message>
        <location filename="../src/views/vnotemainwindow.cpp" line="1639"/>
        <source>Your audio recording device does not work.</source>
        <translation>A hangrögzítő eszköz nem működik.</translation>
    </message>
    <message>
        <location filename="../src/views/vnoterecordbar.cpp" line="233"/>
        <source>No recording device detected</source>
        <translation>Nem található rögzítő eszköz</translation>
    </message>
</context>
<context>
    <name>VoiceNoteItem</name>
    <message>
        <location filename="../src/views/voicenoteitem.cpp" line="196"/>
        <source>Converting voice to text</source>
        <translation>Hang konvertálása szöveggé</translation>
    </message>
</context>
</TS>